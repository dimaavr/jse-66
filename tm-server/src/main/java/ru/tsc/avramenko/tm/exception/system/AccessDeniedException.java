package ru.tsc.avramenko.tm.exception.system;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! You are denied access to this feature.");
    }

}