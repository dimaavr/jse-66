package ru.tsc.avramenko.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.endpoint.ITaskRestEndpoint;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.avramenko.tm.api.endpoint.ITaskRestEndpoint")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return (taskService.findAll());
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create(@RequestBody final Task task) {
        return taskService.create(task);
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody final List<Task> tasks) {
        tasks.forEach(taskService::create);
        return tasks;
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(@RequestBody final Task task) {
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<Task> saveAll(@RequestBody final List<Task> tasks) {
        tasks.forEach(taskService::save);
        return tasks;
    }

    @Override
    @WebMethod
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll() {
        taskService.clear();
    }

}