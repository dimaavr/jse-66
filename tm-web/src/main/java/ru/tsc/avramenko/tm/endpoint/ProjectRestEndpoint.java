package ru.tsc.avramenko.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.endpoint.IProjectRestEndpoint;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.repository.ProjectRepository;
import ru.tsc.avramenko.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.avramenko.tm.api.endpoint.IProjectRestEndpoint")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return (projectService.findAll());
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(@RequestBody final Project project) {
        return projectService.create(project);
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody final List<Project> projects) {
        projects.forEach(projectService::create);
        return projects;
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(@RequestBody final Project project) {
        return projectService.save(project);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<Project> saveAll(@RequestBody final List<Project> projects) {
        projects.forEach(projectService::save);
        return projects;
    }

    @Override
    @WebMethod
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll() {
        projectService.clear();
    }

}