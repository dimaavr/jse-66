<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task Edit</h1>

<form:form action="/task/edit/${task.id}/" method="POST" modelAttribute="task">
<form:input type="hidden" path="id"/>

<p>
<div>Project Name:</div>
<div>
<form:select path="projectId">
    <form:option value="${null}" label="---  // ----"/>
    <form:options items="${projects}" itemLabel="name" itemValue="id"/>
</form:select>
</div>

<p>
<div>Name:</div>
<div><form:input type="text" path="name"/></div>

<p>
<div>Description:</div>
<div><form:input type="text" path="description"/></div>

<p>
<div>Status:</div>
<div>
<form:select path="status">
    <form:options items="${statuses}" itemLabel="displayName"/>
</form:select></div>

<p>
<div>Start Date:</div>
<div><form:input type="datetime-local" path="startDate"/></div>

<p>
<div>Finish Date:</div>
<div><form:input type="datetime-local" path="finishDate"/></div>

<button type="submit">Save Task</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>