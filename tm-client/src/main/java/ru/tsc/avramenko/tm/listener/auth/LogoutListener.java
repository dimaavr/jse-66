package ru.tsc.avramenko.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.endpoint.SessionEndpoint;
import ru.tsc.avramenko.tm.event.ConsoleEvent;

@Component
public class LogoutListener extends AbstractListener {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "User logout from system.";
    }

    @Override
    @EventListener(condition = "@logoutListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final boolean logout = sessionEndpoint.closeSession(sessionService.getSession());
        if (logout) sessionService.setSession(null);
    }

}