package ru.tsc.avramenko.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.avramenko.tm")
public class LoggerConfiguration {
}